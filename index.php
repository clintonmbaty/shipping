<?php
require_once ('navbar.php')
?>

<div class="site-blocks-cover overlay" style="background-image: url(images/slider/TMK005.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                <div class="s-wrap"><div class="s-move">
                        <div class="slide">
                            <h6 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold threeD2">Worldwide Ocean Freight</h6>
                            <p class="lead text-white">Cost Efficient sea transportation. Linking  African ports to more than 600 ports world wide.</p>
                        </div>
                        <div class="slide">
                            <h6 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold threeD2">Shipping Services</h6>
                            <p class="lead text-white">We serve vessel owners, charterers, cargo owners or their agents; providing comprehensive onshore and offshore solutions.</p>

                        </div>
                        <div class="slide">
                            <h6 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold threeD2">Port Agency</h6>
                            <p class="lead text-white">We specialize in organizing, managing and coordinating all aspects of port calls ahead of the vessel's arrival and clearance with local customs & port authorities once the vessel has sailed.</p>
                        </div>
                        <div class="slide">
                            <h6 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold threeD2">Worldwide Ocean Freight</h6>
                            <p class="lead text-white">Cost Efficient sea transportation. Linking 10 African ports to more than 500 ports globally.</p>
                        </div>
                        <div class="slide">
                            <h6 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold threeD2">Port Agency</h6>
                            <p class="lead text-white">We specialize in organizing, managing and coordinating all aspects of port calls ahead of the vessel's arrival and clearance with local customs & port authorities once the vessel has sailed.</p>
                        </div>
                    </div></div>
                <p><a href="services.php" class="btn btn-primary py-3 px-5 text-white">Services!</a></p>
            </div>
        </div>
    </div>
</div>



<div class="site-section ">
    <div class="container" >
        <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center border-primary">
                <h2 class="mb-0 text-primary">What We Offer</h2>
                <p class="color-black-opacity-5">TMK Shipping Limted offers the following services.</p>
            </div>
        </div>
        <div class="row align-items-stretch">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-ship"></span></div>
                    <div>
                        <h3>Shipping Services</h3>
                        <p>A centralized, comprehensive support system for ship owners, charterers, cargo owners or their agents. 24 hours real time updates.</p>
                        <p class="mb-0"><a href="services.php">Learn More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-h-square"></span></div>
                    <div>
                        <h3>Ocean Freight</h3>
                        <p>We provide global ocean shipping solutions based on time, cost and value. African gateway to Middle East, Europe, America & Asia..</p>
                        <p class="mb-0"><a href="services.php">Learn More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-home"></span></div>
                    <div>
                        <h3>Port Agency</h3>
                        <p>Timely berthing allocation and vessel clearance with port and customs authorities in Eastern, Southern Africa and West Africa. Optimized solutions..</p>
                        <p class="mb-0"><a href="services.php">Learn More</a></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="site-section block-13">
    <!-- <div class="container"></div> -->


    <div class="owl-carousel nonloop-block-13">
        <div>
            <a href="#" class="unit-1 text-center">
                <img src="images/slider/Port%20Agency%20Services.jpg" alt="Image" class="img-fluid">
                <div class="unit-1-text">
                    <h3 class="unit-1-heading">Port Agency</h3>
                    <p class="px-5">TMK Shipping is your reliable port agent in Eastern and Southern Africa.</p>
                </div>
            </a>
        </div>

        <div>
            <a href="#" class="unit-1 text-center">
                <img src="images/slider/dry%20bulk%20001.jpg" alt="Image" class="img-fluid">
                <div class="unit-1-text">
                    <h3 class="unit-1-heading">Dry bulk cargo</h3>
                    <p class="px-5">Optimized handling capabilities for agricultural commodities, clinker & cement.</p>
                </div>
            </a>
        </div>

        <div>
            <a href="#" class="unit-1 text-center">
                <img src="images/slider/Project%20cargo.png" alt="Image" class="img-fluid">
                <div class="unit-1-text">
                    <h3 class="unit-1-heading">Break Bulk Cargo</h3>
                    <p class="px-5">Specialist project cargo handling for OOG machinery & equipment, semi processed steel, bagged cargo etc  .</p>
                </div>
            </a>
        </div>

        <div>
            <a href="#" class="unit-1 text-center">
                <img src="images/slider/shipping.jpg" alt="Image" class="img-fluid">
                <div class="unit-1-text">
                    <h3 class="unit-1-heading">Shipping Agency </h3>
                    <p class="px-5">At TMK Shipping Ltd, we offer time, cost & value based shipping services.</p>
                </div>
            </a>
        </div>

        <div>
            <a href="#" class="unit-1 text-center">
                <img src="images/slider/img_4.jpg" alt="Image" class="img-fluid">
                <div class="unit-1-text">
                    <h3 class="unit-1-heading">Container Shipping</h3>
                    <p class="px-5">Globally integrated ocean freight with priority to space even during peak seasons.</p>
                </div>
            </a>
        </div>


    </div>
</div>




<div class="site-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center border-primary">
                <h2 class="font-weight-light text-primary">More About Services</h2>
                <p class="color-black-opacity-5">We offer the following integrated shipping & Logistics services</p>
            </div>
        </div>
        <div class="row align-items-stretch">

            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary flaticon-sea-ship-with-containers"></span></div>
                    <div>
                        <h3>Ships Agency</h3>
                        <p>TMK Shipping offers a comprehensive range of shipping services for expedited clearance of vessels and cargo in Eastern & Southern Africa. .</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary flaticon-platform"></span></div>
                    <div>
                        <h3>Dry Bulk Shipping</h3>
                        <p> We have specialist teams with deep expertise in shipping & handling of dry bulk cargos of wheat, corn, soybeans, fertilizers, clinker & cement..</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-road"></span></div>
                    <div>
                        <h3>Container Shipping</h3>
                        <p>Competitive ocean freight for all types of dry containers, reefer containers and special purpose built containers.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-bolt"></span></div>
                    <div>
                        <h3>Freight Management</h3>
                        <p>We offer fully integrated multimodal freight management services: from shipment booking, pick-up, customs clearance, freight forwarding up to timely delivery..</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary flaticon-barn"></span></div>
                    <div>
                        <h3> Liquid Bulk Shipping</h3>
                        <p>Customized shipping services for chemical tankers and food grade carriers mainly focusing on vegetable oils..</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                <div class="unit-4 d-flex">
                    <div class="unit-4-icon mr-4"><span class="text-primary fa fa-tasks"></span></div>
                    <div>
                        <h3>Break Bulk Shipping</h3>
                        <p>Proficient handling of cargos transported in bags, corrugated and wooden boxes, barrels, bales and sacks. We also handle steel, equipment, machinery, RoRo cargo etc  .</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="site-blocks-cover overlay inner-page-cover" style="background-image: url(images/hero_bg_2.jpg); background-attachment: fixed;">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">

            <div class="col-sm-7" data-aos="fade-up" data-aos-delay="400">
                <a href="#" class="play-single-big mb-4 d-inline-block popup-vimeo"><span class="icon-play"></span></a>
                <h2 class="text-white font-weight-light mb-5 h1">View Our Services By Watching This Short Video</h2>

            </div>
        </div>
    </div>
</div>


<div class="site-section border-top">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2 class="mb-5 text-black">Try Our Services</h2>
                <p class="mb-0"><a href="services.php" class="btn btn-primary py-3 px-5 text-white">Get Started</a></p>
            </div>
        </div>
    </div>
</div>

<?php
require_once ('footer.php')

?>
