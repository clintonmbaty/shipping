<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Ships Husbandry</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Ships Husbandry</h4>
                        <p class="text-center">A vessel requires much more than simple port agency services when in port. The need always arises for husbandry services, which can sometimes mean the
                            involvement of a different agent other than that which is handling the port call.</p>
                        <p class="text-center">TMK Shipping can help you avoid that by providing professional ship husbandry and crew management services at ports and terminals in Eastern, Southern & West Africa. These services include:.</p>
                        <ul class="servicelist">
                            <li>	Crew handling – meet & greet, hotel bookings, shore pass arrangements, etc.</li>
                            <li>	Crew welfare – doctor, dentist, mail, and prepaid telephone calling cards</li>
                            <li>	Spares clearance and delivery</li>
                            <li>	Inward & outward clearance</li>
                            <li>	Follow-up activities with workshops, contractors etc.</li>
                            <li>	Liaison with local authorities</li>
                            <li>	Communication assistance</li>
                        </ul><br><br><br><br><br>

                        <div class="container text-center pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="custom-pagination">
                                        <span>1</span>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            require_once ('servicelink.php')
            ?>


        </div>
    </div>

<?php
require_once ('footer.php')

?>