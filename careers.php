<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/career.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Careers</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Careers</span></p>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Careers:</h4>
                        <p class="text-center">Our core strategy is in creating efficient and cost effective shipping solutions that advance maritime trade between Africa and the rest of the world </p>
                        <p class="text-center">We are always looking for highly talented candidates and experts to apply a disciplined approach in shipping, logistics, finance, risk management, technology and other administrative support operations necessary for TMK Shipping Ltd.’s functional excellence..</p>
                        <p class="text-center">We always seek highly qualified candidates who will help us in continuously delivering superior value to our clients from a diverse portfolio of industries and sectors across which our specialized shipping and logistics services are needed</p>
                        <p class="text-center">Shipping is a complex business requiring that we selectively recruit only the best candidates who can make the right decisions that add value to the Business of TMK Shipping Ltd.</p>
                        <p class="text-center">To apply spontaneously for any other specific position, please submit your curriculum vitae and a cover letter to our Human Resources department: <a href="#"><span class="__cf_email__" data-cfemail="621b0d1710070f030b0e22060d0f030b0c4c010d0f"> hr.group@tmkshipping.com </span></a> </p>
                        <div class="container text-center pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="custom-pagination">
                                        <span>1</span>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

<?php
require_once ('footer.php')

?>