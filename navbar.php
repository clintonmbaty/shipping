<!DOCTYPE html>
<html lang="en">
<head>
    <title>TMK shipping Limited</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="TMK Shipping Services - Connecting Africa to 600 Ports Globally."/>
    <meta property="og:image" content="images/slider/logo.png"/>
    <meta property="og:description"
          content="Our value added shipping services are committed to safety, compliance, cost effective and fast vessel turnaround and comprehensive freight management solutions. We work for ship owners, charterers, cargo owners or their agents in Eastern, Southern & West Africa."/>



    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">



    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slider.css">
    <link rel="stylesheet" href="js/slider.js">
    <link rel="stylesheet" href="css/sidelink.css">

</head>

<body>

<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-3" role="banner">

        <div class="container">
            <div class="row align-items-center">

                <div class="col-11 col-xl-2">
                    <h1 class="mb-0">
                        <img src="images/slider/logo.png" class="logo">
<!--                        <a href="index.php" class="text-white h2 mb-0">TMK</a>-->
                    </h1>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">

                        <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                            <li <?php if($_SERVER['SCRIPT_NAME']=="/index.php") { ?>  class="active"   <?php   }  ?>><a href="index.php">Home</a></li>
                            <li <?php if($_SERVER['SCRIPT_NAME']=="/about.php") { ?>  class="active"   <?php   }  ?>><a href="about.php">About Us</a></li>
                            <li class="has-children" <?php if($_SERVER['SCRIPT_NAME']=="/services.php") { ?>  class="active"   <?php   }  ?>>
                                <a href="services.php">Services</a>
                                <ul class="dropdown">
                                    <li><a href="shipsport.php">Ships & Port Agency</a></li>
                                    <li><a href="shiphusbandry.php">Ships Husbandry</a></li>
                                    <li><a href="containershipping.php">Container Shipping</a></li>
                                    <li><a href="drybulk.php">Dry Bulk Shipping</a></li>
                                    <li><a href="breakbulk.php">Break Bulk Shipping </a></li>
                                    <li><a href="freight.php">Freight Management </a></li>
                                </ul>
                            </li>
                            <li <?php if($_SERVER['SCRIPT_NAME']=="/sustainability.php") { ?>  class="active"   <?php   }  ?>><a href="sustainability.php">Sustainability</a></li>
                            <li <?php if($_SERVER['SCRIPT_NAME']=="/careers.php") { ?>  class="active"   <?php   }  ?>><a href="careers.php">Careers</a></li>
                            <li <?php if($_SERVER['SCRIPT_NAME']=="/contact.php") { ?>  class="active"   <?php   }  ?>><a href="contact.php">Contact</a></li>
                        </ul>
                    </nav>
                </div>


                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

            </div>

        </div>
</div>

</header>









<!--/.Navbar-->

<!--<header>-->
<!--  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">-->
<!--    <ol class="carousel-indicators">-->
<!--      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>-->
<!--      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>-->
<!--      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
<!--    </ol>-->
<!--    <div class="carousel-inner" role="listbox">-->
<!--      &lt;!&ndash; Slide One - Set the background image for this slide in the line below &ndash;&gt;-->
<!--      <div class="carousel-item active" style="background-image: url('images/slider/TMK005.jpg')">-->
<!--        <div class="carousel-caption d-none d-md-block">-->
<!--          <h2 class="display-4">Worldwide Ocean Freight</h2>-->
<!--          <p class="lead"> Cost effective sea transportation. Linking 10 African ports to more than 500 ports globally.</p>-->
<!--        </div>-->
<!--      </div>-->
<!--      &lt;!&ndash; Slide Two - Set the background image for this slide in the line below &ndash;&gt;-->
<!--      <div class="carousel-item" style="background-image: url('images/slider/TMK 002.jpg')">-->
<!--        <div class="carousel-caption d-none d-md-block">-->
<!--          <h2 class="display-4">Shipping Services</h2>-->
<!--          <p class="lead">  We serve vessel owners, charterers or their agents; providing comprehensive shore and offshore solutions..</p>-->
<!--        </div>-->
<!--      </div>-->
<!--      &lt;!&ndash; Slide Three - Set the background image for this slide in the line below &ndash;&gt;-->
<!--      <div class="carousel-item" style="background-image: url('images/slider/TMK 001.jpg')">-->
<!--        <div class="carousel-caption d-none d-md-block">-->
<!--          <h2 class="display-4">Port Agency</h2>-->
<!--          <p class="lead"> We specialize in organizing, managing and coordinating all aspects of port calls ahead of the vessel's arrival and clearance with local port authorities once the vessel has sailed..</p>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">-->
<!--      <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<!--      <span class="sr-only">Previous</span>-->
<!--    </a>-->
<!--    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">-->
<!--      <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<!--      <span class="sr-only">Next</span>-->
<!--    </a>-->
<!--  </div>-->
<!--</header>-->






