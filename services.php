<?php
require_once ('navbar.php')

?>


<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row align-items-center justify-content-center text-center">

      <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
        <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
        <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Services</span></p>
      </div>
    </div>
  </div>
</div>


    <div class="site-section bg-light">
            <div class="row">
                <div class="col-sm-8 mb-5">
                    <div class="p-5 bg-white">
                        <h4 class="text-center text-primary">SERVICES</h4><hr>

<div class="container justify-content-center align-content-center">
    <div class="col-sm-6">
        <p><a href="services.php" class="btn btn-primary servicebtn">All Services</a></p>
        <p> <a href="shipsport.php" class="btn btn-primary servicebtn">Ship & Port Agency</a></p>
        <p ><a href="shiphusbandry.php" class="btn btn-primary servicebtn">Ship Husbandry</a></p>
        <p ><a href="containershipping.php" class="btn btn-primary servicebtn">Conatiner Shipping</a></p>
        <p ><a href="drybulk.php" class="btn btn-primary servicebtn">Dry Bulk Shipping</a></p>
        <p ><a href="breakbulk.php" class="btn btn-primary servicebtn">Break Bulk Shipping</a></p>
        <p ><a href="freight.php" class="btn btn-primary servicebtn">Freight Management</a></p>
    </div>
</div><hr><br/><br/><br/><br/><br/><br/><br/><br/>
                    </div>
                </div>
                <?php
                require_once ('servicelink.php')
                ?>
            </div>
    </div>



<?php
require_once ('footer.php')

?>