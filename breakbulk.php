<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Break Bulk Shipping </span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Break Bulk Shipping </h4>
                        <p class="text-center">TMK Shipping offers end-to-end break bulk shipment coordination, timeliest advice on stevedoring, cargo handling and port costs among other auxiliary requirements in Eastern, Southern & West Africa. </p>
                        <p class="text-center">We have professional commercial agency for break-bulk liners. Our human capital and manpower is highly qualified, have extensive local expertise across Africa and will endeavor to meet your every service requirement. Our specialized break bulk division focuses on the shipment of project cargo, out of gauge cargoes including heavy lifts up to more than 1500mt. </p>
                        <p class="text-center">Through TMKS Shipping’s world-wide network of stevedores, port captains and ships agents, we can ensure smooth operation from anywhere in the world to any of your chosen destination across Eastern, Southern & West Africa.</p>
                        <p class="text-center">The break bulk shipments handled by TMK Shipping include steel billets, steel coils, plates & pipes, wind turbines, generators, railcars, mining machinery & equipment among other bagged, boxed, crated cargo.</p>
                        <br><br><br><br>
                        <div class="container text-center pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="custom-pagination">
                                        <span>1</span>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php
            require_once ('servicelink.php')
            ?>
        </div>
    </div>

<?php
require_once ('footer.php')

?>