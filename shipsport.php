<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Ships & Port Agency</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                <div  class="p-5 bg-white">
                    <h4 class="text-center text-primary">Ships & Port Agency</h4>
                    <p class="text-center">Time and money are the most important considerations for ship owners, masters and operators.
                        The strategic position of Mombasa makes it an ideal transit hub for vessels. TMK
                        Shipping service focus on efficiency and cost saving:</p>
                    <ul class="servicelist">
                        <li>Crew Changes, including arranging visas and entry formalities.</li>
                        <li>	Ship spares delivery</li>
                        <li>	Supply of fresh provisions, deck and engine stores</li>
                        <li>	Nitrogen purging and supply of other gasses</li>
                        <li>	Bunker co-ordination / supply including arranging bunkers quantity/quality surveys</li>
                        <li>	Emergency tugs /towage in the event of engine failure</li>
                        <li>	Ship to Ship transfer operations</li>
                        <li>	Lube oil supply</li>
                        <li>	Deslopping / waste oil removal</li>
                        <li>	Debunkering off-specification bunkers</li>
                        <li>	Gas Carriers</li>
                        <li>	Aerial photography</li>
                        <li>	Medical evacuation by helicopter/OPL launch</li>
                        <li>	Emergency repairs</li>
                        <li>	Floating crane services</li>
                        <li>	Underwater diving services</li>

                    </ul>

                    <div class="container text-center pb-5">
                        <div class="row">
                            <div class="col-12">
                                <p class="custom-pagination">
                                    <span>1</span>
                                    <a href="#">2</a>
                                    <a href="#">3</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
            <?php
            require_once ('servicelink.php')
            ?>

    </div>

    </div>

<?php
require_once ('footer.php')

?>