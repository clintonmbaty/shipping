<footer class="site-footer" style="background-color: #293F86">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-9">
                <div class="row justify-content-center text-center">
                    <div class="col-sm-4">
                        <h2 class="footer-heading mb-4">Quick Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="services.php">Services</a></li>
                            <li><a href="careers.php">Careers</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                            <li><a href="sustainability.php">Sustainability</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="footer-heading mb-4">Services</h2>
                        <ul class="list-unstyled">
                            <li><a href="shipsport.php">Ships & Port Agency</a></li>
                            <li><a href="shiphusbandry.php">Ships Husbandry</a></li>
                            <li><a href="containershipping.php">Container Shipping</a></li>
                            <li><a href="drybulk.php">Dry Bulk Shipping</a></li>
                            <li><a href="breakbulk.php">Break Bulk Shipping </a></li>
                            <li><a href="freight.php">Freight Management </a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="footer-heading mb-4">Follow Us</h2>
                        <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
                <form action="#" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | TMK Shipping Limited
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>

        </div>
    </div>
</footer>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>

</body>
</html>