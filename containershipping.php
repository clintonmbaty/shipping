<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Container Shipping</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Container Shipping</h4>
                        <p class="text-center">TMK Shipping offers reliable, efficient, safe, and cost-effective container shipping.:</p>
                        <p class="text-center">Every cargo deserves personal care and truly top-notch service. As an independent shipping services company, TMK Shipping has the flexibility to cater to your special requirements, whatever they may be. We have the local African knowledge, experience & expertise to ensure that commodities and all types of shipments receive first-class treatment.</p>
                        <p class="text-center">From the initial booking to the final destination & delivery, TMK Shipping combines cutting-edge technology with excellence in human capital to make sure that all consignments arrive on time and in perfect condition. </p>
                        <p class="text-center">Through the representation by TMK Shipping of a variety of international niche carriers and global shipping lines, we offer a wide range of container types for dry and reefer cargo to the main ports in Eastern, Southern & West Africa as well as to smaller ports outside the main trade routes through specialized and dedicated services.</p>
                        <div class="container text-center pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="custom-pagination">
                                        <span>1</span>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            require_once ('servicelink.php')
            ?>


        </div>
    </div>

<?php
require_once ('footer.php')

?>