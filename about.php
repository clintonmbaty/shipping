<?php
require_once ('navbar.php')

?>
  

    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/aboutus/about.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
            <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">About Us</h1>
            <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>About Us</span></p>
          </div>
        </div>
      </div>
    </div>  

    

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          
          <div class="col-md-5 ml-auto mb-5 order-md-2" data-aos="fade">
            <img src="images/aboutus/about2.jpg" alt="Image" class="img-fluid rounded">
          </div>
          <div class="col-md-6 order-md-1" data-aos="fade">
            <div class="text-left pb-1 border-primary mb-4">
              <h2 class="text-primary">Our Heritage</h2>
            </div>
            <p>Our vision and foundation is anchored in provision of professional shipping and maritime management across Africa’
                s key sea ports. Initially established as Tanzania, Mozambique & Kenya Shipping Company, TMK Shipping is currently on the
                right path in its mission of becoming a leading brand across Africa’s shipping & maritime industry.</p>
            <p >We are focused on safety, fast, and efficient vessel turn around and offer a wide range of value added services.</p>

            <div class="row">
              <h2 class="text-primary">Why TMK Shipping</h2>
<p>We understand the importance of a swift port call. With a single point of contact, we can guarantee you efficient service of the highest standard at main sea ports of Eastern & Southern Africa.</p>
          </div>
          
        </div>
      </div>
    </div>
  





        <div class="site-section bg-image overlay" style="background-image: url('images/hero_bg_4.jpg');">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 text-center border-primary">
                        <h2 class="font-weight-light text-primary" data-aos="fade">HOW WE WORK </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
                        <div class="how-it-work-item">
                            <span class="number">1</span>
                            <div class="how-it-work-body">
                                <h2>Serving All Types of Ships</h2>
                                <p class="mb-5">TMK Shipping Ltd is an independent shipping service providers. With our established partners, carefully picked by us, we can assure your ship and crew a personal, full service experience
                                    in every port in Eastern, Southern & West Africa on any day of the year.</p>
                                <p class="mb-5">Ship Agency and comprehensive Logistics for the shipping industry is our passion.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
                        <div class="how-it-work-item">
                            <span class="number">2</span>
                            <div class="how-it-work-body">
                                <h2>TMK Shipping Services to Ships & Crew</h2>
<!--                                <ul class=" list-unstyled white">-->
                                <ul>
                                    <li class="text-white">	Pre arrival notifications</li>
                                    <li class="text-white">	Coordination / ordering of bunker / lubricants Disposal of sludge / slop water</li>
                                    <li class="text-white">	Delivery of spare parts & repairs</li>
                                    <li class="text-white">	Mail / documents / packages to and from ship.</li>
                                    <li class="text-white">	Sending bunker samples and cargo documents</li>
                                    <li class="text-white">	Coordination of launch boat services in Mombasa, Dar Es Salaam, Durban, Beira, Tema and Lagos ports</li>
                                    <li class="text-white">	Ordering of long distance pilotage, sea, and canal pilot</li>
                                    <li class="text-white">	Coordination of inspections</li>
                                    <li class="text-white">	Letter of invitation arrangements to crew</li>
                                    <li class="text-white">	Transfer by car / transportation of crew</li>
                                    <li class="text-white">	Accommodation arrangements for crew</li>
                                    <li class="text-white">	Visits to doctor and dentist</li>
                                    <li class="text-white">	Cash to master</li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
                        <div class="how-it-work-item">
                            <span class="number">3</span>
                            <div class="how-it-work-body">
                                <h2>TMK Shipping Freight Management</h2>
                                <p>We provide comprehensive service for importers and exporters with focus on cost effective and timeliest delivery of their cargo.</p>
                               <ul>
                                <li class="text-white">Shipment booking for import and export</li>
                                <li class="text-white">	Coordinating inspections</li>
                                <li class="text-white">	Warehousing & packaging</li>
                                <li class="text-white">	Freight forwarding</li>
                                <li class="text-white">	Customs clearance</li>
                                <li class="text-white">	Trucking</li>
                                <li class="text-white">	Door to door delivery</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="site-section border-bottom">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 text-center border-primary">
                        <h2 class="font-weight-light text-primary" data-aos="fade">WHERE WE WORK</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
                        <div class="person">
                            <img src="images/work/TMK%20Eastern%20Africa.jpg" alt="Image" class="img-fluid rounded mb-5">
                            <h2 class="text-primary">Eastern Africa</h2>
                            <p class="mb-4">Through the ports of Mombasa, Dar Es, Salaam and Tanga, these ports are the main gateway for ocean shipments to and from Kenya, Tanzania, Uganda, Rwanda, Burundi, South Sudan, Democratic Republic of Congo, Zimbabwe, Zambia and Malawi..</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
                        <div class="person">
                            <img src="images/work/TMK%20Southern%20Africa.jpg" alt="Image" class="img-fluid rounded mb-5">
                            <h2 class="text-primary">Southern Africa</h2>
                            <p class="mb-4">We focus mainly on Durban port, Port Elizabeth, Port of Beira and Walvis Bay. Through these ports, we are committed to serve a wide portfolio of vessels and cargo destined to South Africa, Mozambique, Namibia, Botswana, Zambia, Lesotho and Zimbabwe among others.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
                        <div class="person">
                            <img src="images/work/TMK%20West%20Africa.jpg" alt="Image" class="img-fluid rounded mb-5">
                            <h2 class="text-primary">West Africa</h2>
                            <p class="mb-4">The port of Tema provides key import route for Ghana, Burkina Faso, Mali and Niger.

                                TMK Shipping business in West Africa is poised to grow further with establishment of operations at Lagos port, Nigeria and the Port of Nouakchott, Mauritania in 2021.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
<?php
require_once ('footer.php')

?>