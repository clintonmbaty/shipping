<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Freight Management</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Freight Management:</h4>
                        <p class="text-center">Together with an extensive network of global partners and agents, we ensure cost effective logistics operation and timely delivery of cargo. We provide a unique blend of deep rooted logistics expertise, experience and intelligence in Africa together with global knowledge in shipping to ensure a smooth coordination between multiple carriers, NVOCCs, shippers and consignees. </p>
                        <p class="text-center">Our Freight Management division focuses on finding the best solution for our client based on the specific needs of the cargo and the most suitable mode of transport.</p>
                        <p class="text-center">TMK Shipping offers the following integrated freight management services:</p>
                        <ul class="servicelist">
                            <li>Shipment booking (both import export)</li>
                            <li>	Pick up of the goods</li>
                            <li>	Warehousing & packaging</li>
                            <li>	Trucking & distribution</li>
                            <li>	Freight forwarding</li>
                            <li>	Customs clearance</li>
                            <li>	Project cargo logistics</li>
                            <li>	RoRo cargo logistics</li>
                            <li>	Dangerous goods</li>
                        </ul>
                        <div class="container text-center pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="custom-pagination">
                                        <span>1</span>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            require_once ('servicelink.php')
            ?>

        </div>
    </div>

<?php
require_once ('footer.php')

?>