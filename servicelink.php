<div class="col-sm-3">
    <div class="p-4 mb-3 bg-white">
        <p class="mb-0 font-weight-bold text-center">Services</p><hr>
       <ul >
           <li <?php if($_SERVER['SCRIPT_NAME']=="/services.php") { ?>  class="listActive"   <?php   }  ?>><a href="services.php" class="serviceul">All Services</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/shipsport.php") { ?>  class="listActive"   <?php   }  ?>><a href="shipsport.php" class="serviceul">Ship & Port Agency</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/shiphusbandry.php") { ?>  class="listActive"   <?php   }  ?>><a href="shiphusbandry.php" class="serviceul">Ship Husbandry</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/containershipping.php") { ?>  class="listActive"   <?php   }  ?>><a href="containershipping.php" class="serviceul">Conatiner Shipping</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/drybulk.php") { ?>  class="listActive"   <?php   }  ?>><a href="drybulk.php" class="serviceul">Dry Bulk Shipping</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/breakbulk.php") { ?>  class="listActive"   <?php   }  ?>><a href="breakbulk.php" class="serviceul">Break Bulk Shipping</a></li>
           <li <?php if($_SERVER['SCRIPT_NAME']=="/freight.php") { ?>  class="listActive"   <?php   }  ?>><a href="freight.php" class="serviceul">Freight Management</a></li>
       </ul>
        <hr>
        <img src="images/slider/logo.png" class="logo">
        <hr>
        <p class="mb-0 font-weight-bold">Phone</p>
        <p class="mb-4"><a href="#">+254 733 195 205</a></p>
        <p class="mb-4"><a href="#">+254 720 423 061</a></p>

        <p class="mb-0 font-weight-bold">Email Address</p>
        <p class="mb-0"><a href="#">info@tmkshipping.com </a></p>
        <p class="mb-0"><a href="#"> marine@tmkshipping.com  </a></p>
    </div>

</div>