<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/Suitability/sustainability.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Sustainability</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Sustainability</span></p>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section">
        <div class="container">
            <div class="row mb-5">

                <div class="col-md-5 ml-auto mb-5 order-md-2" data-aos="fade">
                    <img src="images/Suitability/sustain.jpg" alt="Image" class="img-fluid rounded"><hr>
                    <ul class="servicelist">
                        <li >	Promotion of skill development and women empowerment.</li>
                        <li >	Safe drinking water, sanitation and hygiene.</li>
                    </ul>
                </div>
                <div class="col-md-6 order-md-1" data-aos="fade">
                    <div class="text-left pb-1 border-primary mb-4">
                        <h2 class="text-primary">Sustainability</h2>
                    </div>
                    <p>At TMK Shipping, we are committed to leadership in responsibility and sustainability across the countries where we operate..</p>
                    <p >Our company seeks to uphold the highest standards of ethical behavior and transparency in all our business undertakings. We provide high quality services and are committed to the health and safety of our employees, customers, communities and environment.</p>
                    <p >TMK Shipping recognizes the importance of the United Nation’s Guiding Principles (UNGP) on Business and Human Rights. We similarly acknowledge the Principles contained in the United Nations Universal Declaration of Human rights (UDHR)..</p>
                    <p >Our corporate policies, structures, processes, and all our commercial activities are in tandem with UNGP & UDHR. TMK Shipping and all its principals, partners & suppliers adhere to strict code of responsible business conduct towards Health, Safety, Human Rights and Environmental impact in Communities where we operate throughout Africa. .</p>
                    <p >A summary of our sustainability approach to business entails some of the projects which we support in East Africa. These are anchored on the following key areas</p>
                    <ul class="servicelist">
                        <li >Health and Safety</li>
                        <li >	Environmental stewardship</li>
                    </ul>
                </div>
            </div>
        </div>


<?php
require_once ('footer.php')

?>