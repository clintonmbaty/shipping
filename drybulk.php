<?php
require_once ('navbar.php')

?>


    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/service/service.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light text-uppercase font-weight-bold threeD">Our Services</h1>
                    <p class="breadcrumb-custom"><a href="index.php">Home</a> <span class="mx-2">&gt;</span> <span>Dry Bulk Shipping</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="container">
                    <div  class="p-5 bg-white">
                        <h4 class="text-center text-primary">Dry Bulk Shipping:</h4>
                        <p class="text-center">TMK Shipping delivers high-quality dry bulk transportation solutions in Eastern, Southern and West Africa. With experience in handling of Open Hatch Gantry Crane dry bulk vessels, TMK Shipping takes pride in offering a complete set of services for moving full load and parcel bulk cargo shipments.</p>
                        <p class="text-center">TMK Shipping provides versatility in stowage options and safeguards for sensitive cargoes with specialized loading equipment. Our local expertise across main sea ports in Kenya, Tanzania, South Africa, Sudan, Djibouti, Mozambique, Namibia & Ghana enables us to handle products such as grains, fertilisers, petcoke, and minerals among others.</p>
                        <br><br><br><br><br><br>
                    </div>
                    <div class="container text-center pb-5">
                        <div class="row">
                            <div class="col-12">
                                <p class="custom-pagination">
                                    <span>1</span>
                                    <a href="#">2</a>
                                    <a href="#">3</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            require_once ('servicelink.php')
            ?>


        </div>
    </div>

<?php
require_once ('footer.php')

?>